## Персональные рекомандации
Учебный проект для курса "MLOps и production в DS исследованиях 3.0"

### Запуск
#### Локальный запуск проекта для разработки

- Скачать проект:
```
git clone https://github.com/JayDee9373/recsys_test.git
```
1. Установить виртуальную среду с python 3.10 любым удобным способом
- В виртуальной среде установить poetry
-
```
pip install poetry
```

2. Установить пакеты зависимостей
```shell
poetry install --no-root
```

При возникновении проблем с lightfm рекомендуется выполнить следующие команды:
```shell
python -m pip install --upgrade pip setuptools wheel
python -m pip install --no-use-pep517 lightfm
```

3. Установить pre-commit

```bash
pre-commit install
```

Каждый коммит будет запускать проверку pre-commit в соответствии с настройками в файлах `.flake8`, `pyproject.toml`, `.pre-commit-config.yaml`

Если pre-commit заблокировал коммит и выдал сообщение об ошибке, нужно проверить детали ошибки, если необходимо, внести изменения в код согласно правилам и попробовать провести коммит  еще раз.

4. Запуск pre-commit из терминала. В папке проекта выполнить команду

```bash
pre-commit run --all-files --show-diff-on-failure
```

pre-commit запускает последовательно линтер и форматер кода, а так же hooks, указанные в конфиге `.pre-commit-config.yaml`

5. Запуск тестов

```bash
python -m pytest
```
