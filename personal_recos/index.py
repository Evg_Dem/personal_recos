from personal_recos.src.data import make_lfm_dataset, load_data, make_xgb_dataset
from personal_recos.src.models import predict_lfm_model, train_lfm_model, train_xgb_model
from personal_recos.src.visualization import visualize

if __name__ == "__main__":
    load_data.main()
    make_lfm_dataset.main()
    train_lfm_model.train_model()
    predict_lfm_model.get_prediction()
    make_xgb_dataset.main()
    train_xgb_model.train_model()
    visualize.xgb_visualize()
