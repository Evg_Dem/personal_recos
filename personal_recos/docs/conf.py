extensions = []

templates_path = ["_templates"]

source_suffix = ".rst"

master_doc = "index"

project = "recsys_test"

version = "0.1"
release = "0.1"

exclude_patterns = ["_build"]

pygments_style = "sphinx"

html_theme = "default"

html_static_path = ["_static"]

htmlhelp_basename = "recsys_testdoc"

latex_elements = {
    "papersize": "letterpaper",
    "pointsize": "10pt",
    "preamble": "",
}

latex_documents = [
    (
        "index",
        "recsys_test.tex",
        "recsys_test Documentation",
        "Eugeniya Demidova",
        "manual",
    ),
]

man_pages = [("index", "recsys_test", "recsys_test Documentation", ["Eugeniya Demidova"], 1)]

texinfo_documents = [
    (
        "index",
        "recsys_test",
        "recsys_test Documentation",
        "Eugeniya Demidova",
        "recsys_test",
        "A short description of the project.",
        "Miscellaneous",
    ),
]
