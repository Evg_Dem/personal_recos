# Исходные файлы с данными
FILE_PATH_USERS = "users.csv"
FILE_PATH_ITEMS = "items.csv"
FILE_PATH_INTERACTIONS = "interactions.csv"

# Пути к директориям с данными
DIR_PATH_DATA_RAW = "data/raw/"
DIR_PATH_DATA_INTERIM_LFM = "data/interim/lfm"
DIR_PATH_DATA_PROCESSED_LFM = "data/processed/lfm"
DIR_PATH_DATA_INTERIM_XGB = "data/interim/xgb"
DIR_PATH_DATA_PROCESSED_XGB = "data/processed/xgb"
DIR_PATH_MODELS = "models"
DIR_PATH_REFERENCES = "references"
DIR_PATH_REPORT = "reports"

# Имена файлов
FILE_NAME_MAPPING_USERS_ITEMS = "mapping_users_items.pkl"
FILE_NAME_WATCHED_MOVIES = "watched_movies.pkl"
FILE_NAME_TRAIN_MATRIX = "train_matrix.pkl"
FILE_NAME_TEST_MATRIX = "test_matrix.pkl"
FILE_NAME_TEST_WITHOUT_COLD = "test_without_cold.pkl"
FILE_NAME_TEST_COLD = "test_cold.pkl"
FILE_NAME_TRAIN = "train.pkl"
FILE_NAME_TEST = "test.pkl"
FILE_NAME_LIGHTFM_PREDICTION = "lightfm_predictions.pkl"
FILE_NAME_LIGHTFM_MODEL = "lightfm_model.pkl"
FILE_NAME_XGB_MODEL = "xgb_model.pkl"
FILE_NAME_FEATURE_IMPORTANCE = "plot_importance.pdf"


# Перечни колонок
USER_FEATURES = ["age", "income", "sex", "kids_flg"]
ITEM_FEATURES = [
    "content_type",
    "release_year",
    "for_kids",
    "age_rating",
    "genres",
    "countries",
    "actors",
    "studios",
]
ID_COLS = ["user_id", "item_id"]
TARGET = "target"
CATEGORICAL_COLS = [
    "age",
    "income",
    "sex",
    "content_type",
    "studios",
    "genres_0",
    "genres_1",
    "genres_2",
    "countries_0",
    "countries_1",
    "actors_0",
    "actors_1",
    "actors_2",
    "actors_3",
]
MULTIWORDS_COLS = {
    "genres": 2,
    "countries": 2,
    "actors": 3,
}
DROP_COLS = ["last_watch_dt", "watched_pct", "total_dur"]
