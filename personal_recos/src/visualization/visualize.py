from matplotlib import pyplot as plt
from sklearn.metrics import roc_auc_score

from personal_recos.src.config import (
    DIR_PATH_MODELS,
    FILE_NAME_XGB_MODEL,
    DIR_PATH_DATA_PROCESSED_XGB,
    FILE_NAME_TEST,
    TARGET,
    FILE_NAME_FEATURE_IMPORTANCE,
    DIR_PATH_REPORT,
    ID_COLS,
)
from personal_recos.src.utils import load_pickle
from sklearn import metrics
import xgboost as xgb

from personal_recos.src.utils.logger import logger


def xgb_visualize():
    xgb_model = load_pickle(f"{DIR_PATH_MODELS}/{FILE_NAME_XGB_MODEL}")
    xgb_test = load_pickle(f"{DIR_PATH_DATA_PROCESSED_XGB}/{FILE_NAME_TEST}")
    x_test, expected_y = xgb_test.drop([TARGET] + ID_COLS, axis=1), xgb_test[TARGET]
    dmatrix_test = xgb.DMatrix(x_test, expected_y)
    predicted_y = xgb_model.predict(dmatrix_test)
    logger.info(f"ROC AUC score = {roc_auc_score(expected_y, predicted_y):.2f}")
    logger.info(metrics.classification_report(expected_y, predicted_y.round(0)))
    logger.info(f"Confusion matrix{metrics.confusion_matrix(xgb_test[TARGET], predicted_y.round(0))}")
    xgb.plot_importance(xgb_model)
    plt.savefig(f"{DIR_PATH_REPORT}/ {FILE_NAME_FEATURE_IMPORTANCE}")
    logger.info(f"Importance save to {DIR_PATH_REPORT}/ {FILE_NAME_FEATURE_IMPORTANCE}")


if __name__ == "__main__":
    xgb_visualize()
