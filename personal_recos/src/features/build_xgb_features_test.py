from unittest import TestCase

import pandas as pd

from personal_recos.src.features.build_xgb_features import split_multiword_column


class TestBuildXGBFeatures(TestCase):
    def test_split_multiword_column(self):
        expected_result = pd.DataFrame(
            columns=["multi_col_0", "multi_col_1"],
            index=[0, 1],
            data=[["ab", "bc"], ["df", "db"]],
        )
        data = pd.Series(name="multi_col", data=["ab,bc,cd", "df,db,sd"])
        result = split_multiword_column(data, 1)
        pd.testing.assert_frame_equal(expected_result, result)
