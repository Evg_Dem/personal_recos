import pandas as pd
from sklearn.preprocessing import LabelEncoder

from personal_recos.src.config import (
    DROP_COLS,
    MULTIWORDS_COLS,
    DIR_PATH_DATA_PROCESSED_XGB,
    FILE_NAME_TRAIN,
    FILE_NAME_TEST,
    DIR_PATH_REFERENCES,
    CATEGORICAL_COLS,
)
from personal_recos.src.utils import load_pickle, save_to_pickle
from personal_recos.src.utils.logger import logger


def split_multiword_column(column: pd.Series, num_meaningful: int) -> pd.DataFrame:
    """Разделение списочных признаков на отдельные колонки"""
    result = column.str.split(pat=",", n=num_meaningful + 1, expand=True)
    result.columns = [f"{column.name}_{i}" for i in range(num_meaningful + 2)]
    return result.iloc[:, :-1]


def fill_missing(data: pd.DataFrame) -> pd.DataFrame:
    """Заполнение пропусков в данных"""
    data.loc[data["for_kids"].isna(), "for_kids"] = 0
    data.loc[data["kids_flg"].isna(), "kids_flg"] = 2
    for col in CATEGORICAL_COLS:
        data[col].fillna("unknown", inplace=True)
    return data


def encode_categorical_columns(data: pd.DataFrame) -> pd.DataFrame:
    """Кодирование категориальных фич"""
    labelencoder = LabelEncoder()
    for col in CATEGORICAL_COLS:
        labelencoder.fit(data[col].values)
        data[col] = labelencoder.transform(data[col])
        dict_encoder = {*enumerate(labelencoder.classes_)}
        save_to_pickle(f"{DIR_PATH_REFERENCES}/dic_{col}.pkl", dict_encoder)
    return data


def generate_featureset(train: pd.DataFrame, test: pd.DataFrame):
    logger.info("Generate featureset ...")
    train = train.drop(DROP_COLS, axis=1)
    test = test.drop(DROP_COLS, axis=1)
    for col, num_meaningful in MULTIWORDS_COLS.items():
        train = pd.concat(
            [
                train.drop(col, axis=1),
                split_multiword_column(train[col], num_meaningful),
            ],
            axis=1,
        )
    for col, num_meaningful in MULTIWORDS_COLS.items():
        test = pd.concat(
            [test.drop(col, axis=1), split_multiword_column(test[col], num_meaningful)],
            axis=1,
        )
    train = fill_missing(train)
    test = fill_missing(test)
    train = encode_categorical_columns(train)
    test = encode_categorical_columns(test)
    logger.info("Saving...")

    save_to_pickle(f"{DIR_PATH_DATA_PROCESSED_XGB}/{FILE_NAME_TRAIN}", train)
    save_to_pickle(f"{DIR_PATH_DATA_PROCESSED_XGB}/{FILE_NAME_TEST}", test)


if __name__ == "__main__":
    xgb_train = load_pickle(f"{DIR_PATH_DATA_PROCESSED_XGB}/{FILE_NAME_TRAIN}")
    xgb_test = load_pickle(f"{DIR_PATH_DATA_PROCESSED_XGB}/{FILE_NAME_TEST}")
    generate_featureset(xgb_train, xgb_test)
