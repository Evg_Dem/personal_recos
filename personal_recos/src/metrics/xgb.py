from sklearn.metrics import roc_auc_score, accuracy_score


def calculate_accuracy(y_pred, test_data):
    """Вычисление метрики accuracy"""
    return "accuracy", accuracy_score(test_data.get_label(), y_pred.round(0))


def calculate_roc_auc_score(y_pred, test_data):
    """Вычисление ROC-AUC метрики"""
    return "roc_auc_score", roc_auc_score(test_data.get_label(), y_pred)
