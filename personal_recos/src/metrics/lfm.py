from lightfm.evaluation import precision_at_k
from personal_recos.src.utils import load_pickle
from personal_recos.src.utils.logger import logger

K = 10


def calculate_metrics():
    train_matrix = load_pickle("../../data/processed/lfm/train_matrix.pkl")
    test_matrix = load_pickle("../../data/processed/lfm/test_matrix.pkl")
    model = load_pickle("../../models/lightfm_model.pkl")

    logger.info(f"Train precision at k={K}: {precision_at_k(model, train_matrix, k=K).mean()}")
    logger.info(f"Test precision at k={K}: {precision_at_k(model, test_matrix, k=K).mean()}")


if __name__ == "__main__":
    calculate_metrics()
