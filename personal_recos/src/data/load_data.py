from personal_recos.src.data.queries.ods_ai import data_raw
from personal_recos.src.config import FILE_PATH_USERS, FILE_PATH_ITEMS, FILE_PATH_INTERACTIONS
from personal_recos.src.utils.logger import logger

RAW_DATA_URL_ITEMS = "https://storage.yandexcloud.net/datasouls-ods/materials/f90231b6/items.csv"
RAW_DATA_URL_USERS = "https://storage.yandexcloud.net/datasouls-ods/materials/6503d6ab/users.csv"
RAW_DATA_URL_INTERACTIONS = "https://storage.yandexcloud.net/datasouls-ods/materials/04adaecc/interactions.csv"


def main():
    """load datasets from ods.ai"""
    logger.info("Start loading datasets...")
    data_raw.download_raw_files(RAW_DATA_URL_USERS, FILE_PATH_USERS)
    data_raw.download_raw_files(RAW_DATA_URL_ITEMS, FILE_PATH_ITEMS)
    data_raw.download_raw_files(RAW_DATA_URL_INTERACTIONS, FILE_PATH_INTERACTIONS)
    logger.info("Loading datasets complete")


if __name__ == "__main__":
    main()
