import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split

from personal_recos.src.config import (
    DIR_PATH_DATA_RAW,
    FILE_PATH_USERS,
    FILE_PATH_ITEMS,
    USER_FEATURES,
    ITEM_FEATURES,
    DIR_PATH_DATA_PROCESSED_LFM,
    DIR_PATH_DATA_PROCESSED_XGB,
    FILE_NAME_LIGHTFM_PREDICTION,
    FILE_NAME_TEST_WITHOUT_COLD,
    FILE_NAME_TRAIN,
    FILE_NAME_TEST,
)
from personal_recos.src.features.build_xgb_features import generate_featureset
from personal_recos.src.utils import get_data, load_pickle, save_to_pickle
from personal_recos.src.utils.logger import logger

NEGATIVE_CLASS_SIZE = 0.2
TEST_SIZE = 0.3
IS_SHUFFLE_DATASET = False


def generate_positive_labels(dataset_predictions: pd.DataFrame, test: pd.DataFrame) -> pd.DataFrame:
    """Генерирует метки положительного класса"""
    target_positive = pd.merge(dataset_predictions, test, how="inner", on=["user_id", "item_id"])
    target_positive["target"] = 1
    return target_positive


def generate_negative_labels(dataset_predictions: pd.DataFrame, test: pd.DataFrame) -> pd.DataFrame:
    """Генерирует метки отрицательного класса"""
    target_negative = pd.merge(dataset_predictions, test, how="left", on=["user_id", "item_id"])
    target_negative = target_negative.loc[target_negative["watched_pct"].isnull()].sample(frac=NEGATIVE_CLASS_SIZE)
    target_negative["target"] = 0
    return target_negative


def prepare_dataset(
    user_ids: np.ndarray,
    users: pd.DataFrame,
    items: pd.DataFrame,
    predictions: pd.DataFrame,
    test: pd.DataFrame,
) -> pd.DataFrame:
    """Подготовка датасета"""
    target_positive = generate_positive_labels(predictions, test)
    target_negative = generate_negative_labels(predictions, test)

    labeled_data = pd.concat(
        [
            target_positive.loc[target_positive["user_id"].isin(user_ids)],
            target_negative.loc[target_negative["user_id"].isin(user_ids)],
        ]
    )

    labeled_data = pd.merge(labeled_data, users[["user_id"] + USER_FEATURES], how="left", on=["user_id"])
    labeled_data = pd.merge(labeled_data, items[["item_id"] + ITEM_FEATURES], how="left", on=["item_id"])
    labeled_data["user_id"] = labeled_data["user_id"].astype(int)
    labeled_data["item_id"] = labeled_data["item_id"].astype(int)
    return labeled_data


def main():
    logger.info("Generating xgb dataset ...")
    users = get_data(f"{DIR_PATH_DATA_RAW}{FILE_PATH_USERS}")
    items = get_data(f"{DIR_PATH_DATA_RAW}{FILE_PATH_ITEMS}")
    predictions_without_cold = load_pickle(f"{DIR_PATH_DATA_PROCESSED_LFM}/{FILE_NAME_LIGHTFM_PREDICTION}")
    test_without_cold = load_pickle(f"{DIR_PATH_DATA_PROCESSED_LFM}/{FILE_NAME_TEST_WITHOUT_COLD}")

    predictions_without_cold["rank"] = predictions_without_cold.groupby("user_id").cumcount() + 1
    train_users, test_users = train_test_split(
        test_without_cold["user_id"].unique(),
        test_size=TEST_SIZE,
        shuffle=IS_SHUFFLE_DATASET,
    )

    xgb_train = prepare_dataset(train_users, users, items, predictions_without_cold, test_without_cold)
    xgb_test = prepare_dataset(test_users, users, items, predictions_without_cold, test_without_cold)
    logger.info("Saving data ...")

    save_to_pickle(f"{DIR_PATH_DATA_PROCESSED_XGB}/{FILE_NAME_TRAIN}", xgb_train)
    save_to_pickle(f"{DIR_PATH_DATA_PROCESSED_XGB}/{FILE_NAME_TEST}", xgb_test)
    generate_featureset(xgb_test, xgb_test)


if __name__ == "__main__":
    main()
