from typing import Any

import numpy as np
import pandas as pd
from lightfm.data import Dataset
from scipy.sparse import coo_matrix

from personal_recos.src.config import (
    DIR_PATH_DATA_RAW,
    FILE_PATH_INTERACTIONS,
    DIR_PATH_DATA_INTERIM_LFM,
    DIR_PATH_DATA_PROCESSED_LFM,
    FILE_NAME_MAPPING_USERS_ITEMS,
    FILE_NAME_WATCHED_MOVIES,
    FILE_NAME_TRAIN_MATRIX,
    FILE_NAME_TEST_MATRIX,
    FILE_NAME_TEST_WITHOUT_COLD,
    FILE_NAME_TEST_COLD,
)
from personal_recos.src.utils import get_data, save_to_pickle
from personal_recos.src.utils.logger import logger

TEST_DAYS = 30


def create_mapping(data: pd.DataFrame) -> dict[Any]:
    """Генерирует маппинг данных по пользователям и фильмам"""
    dataset = Dataset()
    dataset.fit(data["user_id"].unique(), data["item_id"].unique())
    lightfm_mapping = dataset.mapping()
    lightfm_mapping = {
        "users_mapping": lightfm_mapping[0],
        "user_features_mapping": lightfm_mapping[1],
        "items_mapping": lightfm_mapping[2],
        "item_features_mapping": lightfm_mapping[3],
    }
    lightfm_mapping["users_inv_mapping"] = {v: k for k, v in lightfm_mapping["users_mapping"].items()}
    lightfm_mapping["items_inv_mapping"] = {v: k for k, v in lightfm_mapping["items_mapping"].items()}
    return lightfm_mapping


def train_test_split(data: pd.DataFrame, test_days: int) -> [pd.DataFrame, pd.DataFrame]:
    """Делит данные на тренировочную и тестовую выборки по дате"""
    train = data[data["last_watch_dt"] <= data["last_watch_dt"].max() - pd.Timedelta(test_days, "d")]
    test = data[data["last_watch_dt"] > data["last_watch_dt"].max() - pd.Timedelta(test_days, "d")]
    return train, test


def generate_coo_matrix(data: pd.DataFrame, mapping: dict) -> coo_matrix:
    """Генерирует разреженную матрицу взаимодействия в COO формате"""
    shape = (len(mapping["users_mapping"]), len(mapping["items_mapping"]))
    return coo_matrix(
        (
            np.ones((data["user_id"].shape[0])),
            (
                data["user_id"].map(mapping["users_mapping"]).astype(int),
                data["item_id"].map(mapping["items_mapping"]).astype(int),
            ),
        ),
        shape=shape,
    )


def main():
    logger.info("Make lightFM dataset")
    interactions = get_data(DIR_PATH_DATA_RAW + FILE_PATH_INTERACTIONS, datetime_col=["last_watch_dt"])
    interactions["watched_pct"] = interactions["watched_pct"].astype(pd.Int8Dtype())
    mapping_users_items = create_mapping(interactions)
    train, test = train_test_split(interactions, TEST_DAYS)
    test = test.loc[test["user_id"].isin(train.user_id.unique())]
    test_cold = test.loc[~test["user_id"].isin(train.user_id.unique())]
    watched_movies = train.groupby("user_id")["item_id"].apply(list).to_dict()
    logger.info("Generate COO matrix")
    train_matrix = generate_coo_matrix(train, mapping_users_items)
    test_matrix = generate_coo_matrix(test, mapping_users_items)
    logger.info("Saving data ...")
    save_to_pickle(
        f"{DIR_PATH_DATA_INTERIM_LFM}/{FILE_NAME_MAPPING_USERS_ITEMS}",
        mapping_users_items,
    )
    save_to_pickle(f"{DIR_PATH_DATA_INTERIM_LFM}/{FILE_NAME_WATCHED_MOVIES}", watched_movies)
    save_to_pickle(f"{DIR_PATH_DATA_PROCESSED_LFM}/{FILE_NAME_TRAIN_MATRIX}", train_matrix)
    save_to_pickle(f"{DIR_PATH_DATA_PROCESSED_LFM}/{FILE_NAME_TEST_MATRIX}", test_matrix)
    save_to_pickle(f"{DIR_PATH_DATA_PROCESSED_LFM}/{FILE_NAME_TEST_WITHOUT_COLD}", test)
    save_to_pickle(f"{DIR_PATH_DATA_PROCESSED_LFM}/{FILE_NAME_TEST_COLD}", test_cold)


if __name__ == "__main__":
    main()
