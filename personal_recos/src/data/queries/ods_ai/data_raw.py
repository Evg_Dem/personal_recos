import os.path

from personal_recos.src.config import DIR_PATH_DATA_RAW
from personal_recos.src.utils.downloader import download_file


def download_raw_files(url: str, file_name: str) -> None:
    if not os.path.exists(DIR_PATH_DATA_RAW + file_name):
        download_file(url, f"{DIR_PATH_DATA_RAW}/{file_name}")
