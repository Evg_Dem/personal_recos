import os.path
import pickle
from typing import Any

import pandas as pd

from personal_recos.src.data import load_data


def get_data(file_path: str, datetime_col: list = None) -> pd.DataFrame:
    """Load data from csv and drop duplicates"""
    if not os.path.exists(file_path):
        load_data.main()
    return pd.read_csv(file_path, parse_dates=datetime_col)


def load_pickle(file_path: str) -> Any:
    with open(file_path, "rb") as input_file:
        return pickle.load(input_file)


def save_to_pickle(file_path: str, file_content: Any) -> None:
    with open(file_path, "wb") as input_file:
        pickle.dump(file_content, input_file)
