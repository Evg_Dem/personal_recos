from urllib import request


def download_file(url: str, destination: str):
    request.urlretrieve(url, destination)
