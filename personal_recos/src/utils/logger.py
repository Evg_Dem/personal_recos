"""
Сущность задает настройки для логирования
"""
import logging
from pythonjsonlogger import jsonlogger

logger = logging.getLogger()

log_handler = logging.StreamHandler()
formatter = jsonlogger.JsonFormatter()
log_handler.setFormatter(formatter)
logger.addHandler(log_handler)
logger.setLevel(level=logging.INFO)
