import xgboost as xgb

from personal_recos.src.config import (
    DIR_PATH_DATA_PROCESSED_XGB,
    FILE_NAME_TRAIN,
    FILE_NAME_TEST,
    DIR_PATH_MODELS,
    FILE_NAME_XGB_MODEL,
    TARGET,
    ID_COLS,
)
from personal_recos.src.metrics.xgb import calculate_roc_auc_score

from personal_recos.src.utils import load_pickle, save_to_pickle
from personal_recos.src.utils.logger import logger

PARAMETERS_XGB = {
    "objective": "binary:logistic",
    "colsample_bytree": 0.7,
    "gamma": 2,
    "learning_rate": 0.3,
    "max_depth": 7,
    "reg_lambda": 10,
    "scale_pos_weight": 2.47,
    "subsample": 0.6,
    "alpha": 0.01,
    "random_state": 23,
}


def train_model():
    logger.info("Preparing DMatrix ...")

    train = load_pickle(f"{DIR_PATH_DATA_PROCESSED_XGB}/{FILE_NAME_TRAIN}")
    test = load_pickle(f"{DIR_PATH_DATA_PROCESSED_XGB}/{FILE_NAME_TEST}")

    x_train, y_train = train.drop([TARGET] + ID_COLS, axis=1), train[TARGET]
    x_test, y_test = test.drop([TARGET] + ID_COLS, axis=1), test[TARGET]

    dmatrix_train = xgb.DMatrix(x_train, y_train, enable_categorical=True)
    dmatrix_test = xgb.DMatrix(x_test, y_test, enable_categorical=True)
    evals_result = {}
    watch_list = [(dmatrix_train, "train"), (dmatrix_test, "test")]
    logger.info("Training model ...")
    xgb_model = xgb.train(
        PARAMETERS_XGB,
        dmatrix_train,
        1000,
        watch_list,
        maximize=True,
        early_stopping_rounds=20,
        verbose_eval=7,
        evals_result=evals_result,
        custom_metric=calculate_roc_auc_score,
    )
    logger.info("Training complete ...")
    save_to_pickle(f"{DIR_PATH_MODELS}/{FILE_NAME_XGB_MODEL}", xgb_model)


if __name__ == "__main__":
    train_model()
