import numpy as np
import pandas as pd

from personal_recos.src.config import (
    FILE_NAME_MAPPING_USERS_ITEMS,
    DIR_PATH_MODELS,
    FILE_NAME_LIGHTFM_MODEL,
    DIR_PATH_DATA_PROCESSED_LFM,
    FILE_NAME_TEST_WITHOUT_COLD,
    DIR_PATH_DATA_INTERIM_LFM,
    FILE_NAME_WATCHED_MOVIES,
    FILE_NAME_LIGHTFM_PREDICTION,
)
from personal_recos.src.utils import load_pickle, save_to_pickle

TOP_N = 20  # топ фильмов, предложенных моделью
NUM_THREADS = 8  # число потоков


def generate_lightfm_recs_mapper(
    model: object,
    item_ids: list,
    known_items: dict,
    user_features: list | None,
    item_features: list | None,
    n: int,
    user_mapping: dict,
    item_inv_mapping: dict,
    num_threads: int = 4,
):
    """Маппер для интерпретации предсказания модели LightFM"""

    def _recs_mapper(user):
        user_id = user_mapping[user]
        recs = model.predict(
            user_id,
            item_ids,
            user_features=user_features,
            item_features=item_features,
            num_threads=num_threads,
        )

        additional_n = len(known_items[user_id]) if user_id in known_items else 0
        total_n = n + additional_n
        top_cols = np.argpartition(recs, -np.arange(total_n))[-total_n:][::-1]

        final_recs = [item_inv_mapping[item] for item in top_cols]
        if additional_n > 0:
            filter_items = known_items[user_id]
            final_recs = [item for item in final_recs if item not in filter_items]
        return final_recs[:n]

    return _recs_mapper


def get_prediction():
    model = load_pickle(f"{DIR_PATH_MODELS}/{FILE_NAME_LIGHTFM_MODEL}")
    mapping_users_items = load_pickle(f"{DIR_PATH_DATA_INTERIM_LFM}/{FILE_NAME_MAPPING_USERS_ITEMS}")
    lightfm_test = load_pickle(f"{DIR_PATH_DATA_PROCESSED_LFM}/{FILE_NAME_TEST_WITHOUT_COLD}")
    watched_movies = load_pickle(f"{DIR_PATH_DATA_INTERIM_LFM}/{FILE_NAME_WATCHED_MOVIES}")

    lightfm_predictions = pd.DataFrame({"user_id": lightfm_test["user_id"].unique()})
    item_ids = list(mapping_users_items["items_mapping"].values())

    mapper = generate_lightfm_recs_mapper(
        model,
        item_ids=item_ids,
        known_items=watched_movies,
        n=TOP_N,
        user_features=None,
        item_features=None,
        user_mapping=mapping_users_items["users_mapping"],
        item_inv_mapping=mapping_users_items["items_inv_mapping"],
        num_threads=NUM_THREADS,
    )
    lightfm_predictions["item_id"] = lightfm_predictions["user_id"].map(mapper)
    lightfm_predictions = lightfm_predictions.explode("item_id").reset_index(drop=True)
    save_to_pickle(
        f"{DIR_PATH_DATA_PROCESSED_LFM}/{FILE_NAME_LIGHTFM_PREDICTION}",
        lightfm_predictions,
    )


if __name__ == "__main__":
    get_prediction()
