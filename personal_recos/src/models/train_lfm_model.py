import pandas as pd
from lightfm import LightFM

from personal_recos.src.config import (
    DIR_PATH_DATA_PROCESSED_LFM,
    FILE_NAME_TRAIN_MATRIX,
    DIR_PATH_MODELS,
    FILE_NAME_LIGHTFM_MODEL,
)
from personal_recos.src.utils import save_to_pickle
from personal_recos.src.utils.logger import logger

PARAMETERS = {
    "no_components": 64,
    "learning_rate": 0.03,
    "max_sampled": 6,
    "random_state": 32,
    "loss": "warp",
    "epochs": 32,
    "num_threads": 4,
}


def train_model():
    train_matrix = pd.read_pickle(f"{DIR_PATH_DATA_PROCESSED_LFM}/{FILE_NAME_TRAIN_MATRIX}")
    logger.info("Training model ...")
    model = LightFM(
        loss=PARAMETERS["loss"],
        no_components=PARAMETERS["no_components"],
        learning_rate=PARAMETERS["learning_rate"],
        max_sampled=PARAMETERS["max_sampled"],
        random_state=PARAMETERS["random_state"],
    )
    model.fit(train_matrix, epochs=PARAMETERS["epochs"], num_threads=PARAMETERS["num_threads"])
    save_to_pickle(f"{DIR_PATH_MODELS}/{FILE_NAME_LIGHTFM_MODEL}", model)
    logger.info("Training complete ...")


if __name__ == "__main__":
    train_model()
